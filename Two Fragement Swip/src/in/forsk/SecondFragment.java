package in.forsk;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;

public class SecondFragment extends Fragment {
	// alt+shift+s to open override/implemented methods of super class
	public SecondFragment() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.fragment_second, container, false);
		Button backBtn	=	(Button)rootView.findViewById(R.id.button1);
		backBtn.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				getActivity().getFragmentManager().beginTransaction().replace(R.id.container, new MainActivity.PlaceholderFragment()).commit();
			}
		});
		return rootView;
	}

}
