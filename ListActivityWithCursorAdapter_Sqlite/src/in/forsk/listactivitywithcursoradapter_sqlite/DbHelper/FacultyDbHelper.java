package in.forsk.listactivitywithcursoradapter_sqlite.DbHelper;

import in.forsk.listactivitywithcursoradapter_sqlite.DbAdapter.DBAdapter;
import in.forsk.listactivitywithcursoradapter_sqlite.wrapper.FacultyWrapper;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;

public class FacultyDbHelper {

	public static void insertRecords(Context context, FacultyWrapper _obj) {
		long id = 0;

		if (updateRecordsPID(context, _obj))
			return;

		ContentValues initialValues = ObjectToContentValue(_obj);

		id = DBAdapter.getInstance(context).insertRecordsInDB(DBAdapter.FACULTY_TABLE_NAME, null, initialValues);
		_obj = null;

	}

	public static boolean updateRecordsPID(Context context, FacultyWrapper _obj) {

		ContentValues initialValues = ObjectToContentValue(_obj);

		String[] strArray = { "" + _obj.PID };

		long n = DBAdapter.getInstance(context).updateRecordsInDB(DBAdapter.FACULTY_TABLE_NAME, initialValues, DBAdapter.FACULTY_COLUMN_ID+"=?", strArray);

		return n > 0;
	}

	public static void deleteRecords(Context context) {

		DBAdapter.getInstance(context).deleteRecordInDB(DBAdapter.FACULTY_TABLE_NAME, null, null);

	}

	public static Cursor retriveRecords(Context context) {
		Cursor c = DBAdapter.getInstance(context).selectRecordsFromDB(DBAdapter.FACULTY_TABLE_NAME, null, null, null, null, null, null);
		return c;
	}

	public static ContentValues ObjectToContentValue(FacultyWrapper obj) {
		ContentValues initialValues = new ContentValues();

		initialValues.put(DBAdapter.FACULTY_COLUMN_FIRSTNAME, obj.getFirst_name());
		initialValues.put(DBAdapter.FACULTY_COLUMN_LASTNAME, obj.getLast_name());
		initialValues.put(DBAdapter.FACULTY_COLUMN_PHOTO, obj.getPhoto());
		initialValues.put(DBAdapter.FACULTY_COLUMN_DEP, obj.getDepartment());
		initialValues.put(DBAdapter.FACULTY_COLUMN_EMAIL, obj.getEmail());
		initialValues.put(DBAdapter.FACULTY_COLUMN_PHONE, obj.getPhone());

		return initialValues;
	}

}
